/**
 * @description It contains server setup functions.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

// MODULE DEPENDENCIES
const express = require("express");
const path = require("path");
const logger = require("morgan");
const route = require("./routes");
const cors = require("cors");

// UTILS
const { failAction } = require("./utils/response");
const { FAILURE } = require("./utils/status");

const app = express();

// ALLOW ALL CORS
app.use(cors());

// SERVER HTML FILES
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// LOG API CALLS
app.use(logger("dev"));

// ALLOW PAYLOAD
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// SERVE STATIC FILES
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "view")));

// ERROR HANDLER
app.use((err, req, res, next) => {
  if (err?.error?.isJoi) {
    // WE HAD A JOI ERROR, LET'S RETURN A CUSTOM 400 JSON RESPONSE
    res
      .status(FAILURE)
      .json(
        failAction(
          FAILURE,
          err?.error?.message?.toString().replace(/[\""]+/g, "")
        )
      );
  } else {
    // PASS ON TO ANOTHER ERROR HANDLER
    next(err);
  }
});

// SQL CONNECTION
const connection = require("./connection/sql-connection");
connection();

// API ROUTES
app.use("/", route);

module.exports = app;
