/**
 * @description It contains SQL connection setup.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

require("dotenv").config();

const mysql = require("mysql");

// fetch details from ENV file
global.DB = mysql.createPool({
  host: process.env["SQL_HOST"],
  user: process.env["SQL_USER"],
  password: process.env["SQL_PASSWORD"],
  database: process.env["SQL_DATABASE"],
  port: process.env["SQL_PORT"],
  charset: process.env["SQL_CHARSET"],
});

let connection = async function () {
  try {
    await DB.query(
      `create database if not exists ${process.env["SQL_DATABASE"]}`
    );
    await DB.query(`use ${process.env["SQL_DATABASE"]}`);

    await DB.on("error", function (err) {
      console.log("db error", err);
      if (err.code === "PROTOCOL_CONNECTION_LOST") {
        // Connection to the MySQL server is usually
        connection(); // lost due to either server restart, or a
      } else {
        // connection idle timeout (the wait_timeout
        throw err; // server variable configures this)
      }
    });

    await DB.on("enqueue", function (connection) {
      console.log("Waiting for available connection slot");
    });

    await DB.on("acquire", function (connection) {
      console.log("Connection %d acquired", connection.threadId);
    });

    await DB.on("release", function (connection) {
      console.log("Connection %d released", connection.threadId);
    });
  } catch (error) {
    console.log("Error in connecting to database: ", error);
    return error;
  }
};

module.exports = connection;
