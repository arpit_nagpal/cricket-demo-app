/**
 * @description It contains all the common functions
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

/**
 *
 * @param {*} fields
 * @param {*} body
 * @returns
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */
const validateBody = (fields = [], body = {}) => {
  return new Promise((resolve, reject) => {
    let message = "";

    if (!body || !Object.keys(body)?.length) {
      fields.filter((field) => {
        message += `${field?.label}, `;
      });
    }

    Object.keys(body).forEach((key) => {
      // CHECK IF UNKNOWN FIELD IS PROVIDED TO BODY
      if (fields.findIndex((field) => field.label === key) === -1) {
        throw `${key} is not allowed`;
      }
      // CHECK IF ANY REQUIRED FIELD IS MISSING
    });

    fields.filter((field) => {
      if (field.isRequired && !body[field.label] && body[field.label] !== 0) {
        message += `${field.label}, `;
      }
    });

    if (message)
      return reject(
        `Fields ${message.slice(0, message.length - 2)} is required`
      );

    return resolve();
  })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      return err;
    });
};

const sanitizeInput = (data) => {
  data = data.replace(/[^a-z0-9áéíóúñü \.,_-]/gim, "");
  return data.trim();
};

module.exports = { validateBody, sanitizeInput };
