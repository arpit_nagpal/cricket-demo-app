/**
 * @description It contains common response handler functions.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

const status = require("./status");

exports.successAction = successAction;
exports.failAction = failAction;

function successAction(data, message = "OK") {
  return { statusCode: status.SUCCESS, data, message };
}

function failAction(statusCode, message = "Fail") {
  return { statusCode, data: null, message };
}
