/**
 * @description It contains all the static messages.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

exports.HEALTH_CHECK = "App is live.";

// TEAM
exports.TEAM_LIST_SUCCESS = "Successfully fetched team listing";

// PLAYERS
exports.DATA_NOT_FOUND = "{{FIELD_NAME}} not found.";
exports.ADD_PLAYER_HISTORY_SUCCESS = "Successfully added player history.";
exports.NO_FILE = "Please select a file to upload.";
exports.FILE_UPLOAD_ERROR = "Error uploading image.";
exports.FILE_UPLOAD_SUCCESS = "Successfully uploaded image.";
