/**
 * @description It contains function layer for Team controller.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

// SERVICES
const { getTeamService } = require("../../services/cricket/team-service");

// UTILS
const { TEAM_LIST_SUCCESS } = require("../../utils/constants");
const { failAction, successAction } = require("../../utils/response");
const { FAILURE, SUCCESS } = require("../../utils/status");

const getTeamController = async (req, res) => {
  try {
    // GET LIST OF TEAMS
    const teams = await getTeamService({ ...req.body });

    res.status(SUCCESS).json(successAction(teams, TEAM_LIST_SUCCESS));
  } catch (error) {
    res.status(FAILURE).json(failAction(FAILURE, error.message));
  }
};

module.exports = {
  getTeamController,
};
