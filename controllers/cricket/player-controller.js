/**
 * @description It contains function layer for Team controller.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

// SERVICES
const {
  getPlayerDetailsService,
  addPlayerHistoryService,
  updateProfileImageService,
} = require("../../services/cricket/player-service");

// UTILS
const {
  TEAM_LIST_SUCCESS,
  ADD_PLAYER_HISTORY_SUCCESS,
  FILE_UPLOAD_SUCCESS,
} = require("../../utils/constants");
const { failAction, successAction } = require("../../utils/response");
const { FAILURE, SUCCESS } = require("../../utils/status");

// GET PLAYER DETAILS
const getPlayerDetails = async (req, res) => {
  try {
    const player = await getPlayerDetailsService({ ...req.params });

    res.status(SUCCESS).json(successAction(player, TEAM_LIST_SUCCESS));
  } catch (error) {
    res.status(FAILURE).json(failAction(FAILURE, error.message));
  }
};

// ADD PlAYER HISTORY
const addPlayerHistory = async (req, res) => {
  try {
    const player = await addPlayerHistoryService({ ...req.body });

    res.status(SUCCESS).json(successAction(player, ADD_PLAYER_HISTORY_SUCCESS));
  } catch (error) {
    res.status(FAILURE).json(failAction(FAILURE, error.message));
  }
};

// UPDATE PROFILE IMAGE
const updateProfileImage = async (req, res) => {
  try {
    const player = await updateProfileImageService(req, res);

    res.status(SUCCESS).json(successAction(player, FILE_UPLOAD_SUCCESS));
  } catch (error) {
    res.status(FAILURE).json(failAction(FAILURE, error.message));
  }
};

module.exports = {
  getPlayerDetails,
  addPlayerHistory,
  updateProfileImage,
};
