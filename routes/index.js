/**
 * @description It contains all the routes.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

// MODULES
const express = require("express");
const router = express.Router();
const path = require("path");
const cricket = require("./cricket");

// UTILS
const { HEALTH_CHECK } = require("../utils/constants");
const { successAction } = require("../utils/response");
const { SUCCESS } = require("../utils/status");

router.get("/", (req, res) => {
  res.sendFile(path.resolve(__dirname + "/../view/index.html"));
});

router.get("/player*", (req, res) => {
  res.sendFile(path.resolve(__dirname + "/../view/player.html"));
});

router.use("/cricket", cricket);

router.get("/doc", (req, res) => {
  res.sendFile(path.resolve(__dirname + "/../public/document/doc.html"));
});

router.get("/health-check", (req, res) => {
  res.status(SUCCESS).json(successAction(SUCCESS, HEALTH_CHECK));
});

module.exports = router;
