/**
 * @description It contains all the combined routes of cricket.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */
const teams = require("./teams");
const players = require("./players");

module.exports = [teams, players];
