/**
 * @description This is contain teams route.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

const express = require("express");
const {
  getTeamController,
} = require("../../controllers/cricket/team-controller");

const app = express();

app.post("/teams/list", getTeamController);

module.exports = app;
