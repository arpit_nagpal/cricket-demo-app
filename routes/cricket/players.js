/**
 * @description This is contain players route.
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */

const express = require("express");
const {
  getPlayerDetails,
  addPlayerHistory,
  updateProfileImage
} = require("../../controllers/cricket/player-controller");

const app = express();

// GET PLAYER DETAILS
app.get("/players/:id", getPlayerDetails);

// ADD PLAYER HISTORY
app.post("/players/addHistory", addPlayerHistory);

// UPLOAD PLAYER PROFILE
app.post("/players/updateProfileImage", updateProfileImage);

module.exports = app;
