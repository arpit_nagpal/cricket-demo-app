const { validateBody } = require("../../utils/common-functions");
const { failAction } = require("../../utils/response");
const { FAILURE } = require("../../utils/status");

/**
 * @param - used params: page, limit, searchString, sortBy, sort
 * @returns - List of All Teams
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */
const getTeamService = async (body) => {
  try {
    // DECONSTRUCT BODY
    const { page = 1, limit = 10, searchString = "", sortBy, sort } = body;

    // ALLOWED FIELDS
    const validFields = [
      {
        label: "page",
        isRequired: true,
      },
      {
        label: "limit",
        isRequired: true,
      },
      {
        label: "searchString",
      },
      {
        label: "sortBy",
      },
      {
        label: "sort",
      },
    ];

    // VALIDATE BODY
    const validate = await validateBody(validFields, { ...body });
    if (validate) {
      throw failAction(FAILURE, validate);
    }

    const offset = (page - 1) * limit;
    let values = [offset, limit];

    return new Promise((resolve, reject) => {
      let SQL = `
      SELECT
        teams.teamId,
        teams.teamName,
        teams.createdDate as teamCreatedDate,
        teams.updatedDate as teamUpdatedDate,
        players.playerId,
        players.playerName,
        players.playerImage,
        players.playerSpecialization,
        players.matchesPlayed,
        players.runsScored,
        players.totalWickets,
        players.createdDate,
        players.updatedDate
      FROM teams
      LEFT JOIN players ON players.teamId = teams.teamId `;

      // IF ANY SEARCH STRING IS PRESENT
      if (searchString) {
        values = [[searchString.trim()], ...values];
        SQL += `AND players.playerName LIKE CONCAT('%', ?,  '%') `;
      }

      SQL += `ORDER BY teamId ASC `;

      // SORT DATA
      if (sortBy && sort) {
        SQL += `, ${sortBy} ${sort} `;
      }

      SQL += `LIMIT ?, ?`;

      DB.query(SQL, values, (err, result) => {
        if (err) reject(err);
        else {
          // CONVERT ARRAY TO MULTIDIMENSIONAL ARRAY
          const teams = result
            .map((item) => {
              const { teamId, teamName, teamCreatedDate, teamUpdatedDate } =
                item;
              return {
                teamId,
                teamName,
                createdDate: teamCreatedDate,
                updatedDate: teamUpdatedDate,
              };
            })
            .filter(
              (team, i, ar) =>
                ar.findIndex((x) => x.teamId === team.teamId) === i
            )
            .sort((a, b) => a - b)
            .map((item) => {
              const players = result
                .filter((team) => team.teamId == item.teamId)
                .map((team) => {
                  const obj = Object.assign({}, team);
                  delete obj["teamId"];
                  delete obj["teamName"];
                  delete obj["teamCreatedDate"];
                  delete obj["teamUpdatedDate"];

                  if (obj["playerId"]) {
                    return {
                      ...obj,
                    };
                  }
                })
                .filter((x) => x);
              return { ...item, players };
            });

          resolve(teams);
        }
      });
    })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        throw failAction(FAILURE, err.message);
      });
  } catch (error) {
    throw failAction(FAILURE, error.message);
  }
};

module.exports = {
  getTeamService,
};
