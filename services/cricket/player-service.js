const { validateBody, sanitizeInput } = require("../../utils/common-functions");
const { DATA_NOT_FOUND, NO_FILE } = require("../../utils/constants");
const { failAction } = require("../../utils/response");
const { FAILURE } = require("../../utils/status");
const formidable = require("formidable");
const fs = require("fs");
const path = require("path");

/**
 * @param - used params: id
 * @returns - Player Details
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */
const getPlayerDetailsService = async ({ id }) => {
  try {
    // ALLOWED FIELDS
    const validFields = [
      {
        label: "id",
        isRequired: true,
      },
    ];

    // VALIDATE BODY
    const validate = await validateBody(validFields, { id });
    if (validate) {
      throw failAction(FAILURE, validate);
    }

    return new Promise((resolve, reject) => {
      let SQL = `
      SELECT
        players.playerId,
        players.playerName,
        players.playerImage,
        players.playerSpecialization,
        players.matchesPlayed,
        players.runsScored,
        players.totalWickets,
        players.history,
        players.createdDate,
        players.updatedDate
      FROM players
      WHERE players.playerId = ?
    `;

      DB.query(SQL, [id], (err, result) => {
        if (err) reject(err);
        else {
          if (result && result.length) resolve(result[0]);
          else
            reject({
              message: DATA_NOT_FOUND.replace("{{FIELD_NAME}}", "Player"),
            });
        }
      });
    })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        throw failAction(FAILURE, err.message);
      });
  } catch (error) {
    throw failAction(FAILURE, error.message);
  }
};

/**
 * @param - used params: id
 * @returns - Player Details
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */
const addPlayerHistoryService = async (body) => {
  try {
    const { playerId, matchDescription, runs, wickets } = body;

    // ALLOWED FIELDS
    const validFields = [
      {
        label: "playerId",
        isRequired: true,
      },
      {
        label: "matchDescription",
        isRequired: true,
      },
      {
        label: "runs",
        isRequired: true,
      },
      {
        label: "wickets",
        isRequired: true,
      },
    ];

    // VALIDATE BODY
    const validate = await validateBody(validFields, { ...body });
    if (validate) {
      throw failAction(FAILURE, validate);
    }

    return new Promise((resolve, reject) => {
      let SQL = `
      SELECT
        players.matchesPlayed,
        players.runsScored,
        players.totalWickets,
        players.history
      FROM players
      WHERE players.playerId = ?
    `;

      DB.query(SQL, [playerId], (err, result) => {
        if (err) reject(err);
        else {
          if (result && result.length) {
            let { matchesPlayed, runsScored, totalWickets, history } =
              result[0];

            const historyJson =
              typeof history === "string"
                ? JSON.parse(history)
                : history
                ? history
                : [];

            matchesPlayed = parseInt(matchesPlayed) + 1;
            runsScored = parseInt(runsScored) + parseInt(runs);
            totalWickets = parseInt(totalWickets) + parseInt(wickets);

            historyJson.unshift({ matchDescription, runs, wickets });

            // ADD HISTORY
            const historySQL = `
              UPDATE players
              SET 
                history = ?,
                matchesPlayed = ?,
                runsScored = ?,
                totalWickets = ?
              WHERE players.playerId = ?
            `;

            DB.query(
              historySQL,
              [
                JSON.stringify(historyJson),
                matchesPlayed,
                runsScored,
                totalWickets,
                playerId,
              ],
              (error, result) => {
                if (error) reject(error);
                else resolve();
              }
            );
          } else
            reject({
              message: DATA_NOT_FOUND.replace("{{FIELD_NAME}}", "Player"),
            });
        }
      });
    })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        throw failAction(FAILURE, err.message);
      });
  } catch (error) {
    throw failAction(FAILURE, error.message);
  }
};

/**
 *
 * @param {*} req
 * @author Arpit Nagpal <arpit.nagpal08@outlook.com>
 */
const updateProfileImageService = async (req, res) => {
  const { playerId } = req.body;

  // ALLOWED FIELDS
  const validFields = [
    {
      label: "playerId",
      isRequired: true,
    },
    {
      label: "image",
      isRequired: true,
    },
  ];

  return new Promise((resolve, reject) => {
    let fileName;
    let form = new formidable.IncomingForm();
    form.uploadDir = "view/assets/images";
    form.keepExtensions = true;
    form.multiples = false;
    form.on("fileBegin", function (name, file) {
      fileName = sanitizeInput(file.name);
      file.path = path.join(__dirname + "/../../view/assets/images/", fileName);
    });

    form.parse(req, async (err, fields, file) => {
      if (err) {
        return reject(ailAction(FAILURE, err));
      } else {
        // VALIDATE BODY
        const validate = await validateBody(validFields, {
          ...file,
          ...fields,
        });
        if (validate) {
          return reject(failAction(FAILURE, validate));
        }

        const SQL = `
          UPDATE players
          SET playerImage = ?
          WHERE players.playerId = ? 
        `;
        DB.query(SQL, [fileName, fields.playerId], (err, result) => {
          if (err) reject(err);
          else resolve(file?.image?.name);
        });
      }
    });
  })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      throw failAction(FAILURE, err.message);
    });
};

module.exports = {
  getPlayerDetailsService,
  addPlayerHistoryService,
  updateProfileImageService,
};
